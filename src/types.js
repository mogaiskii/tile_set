const types = {
    'e': {color: 'white', bg: 'floor.png', tip: 'empty', key: 'e'},
    'p': {color: 'yellow', bg: 'player.png', tip: "player", key: 'p'},
    'b': {color: 'black', bg: 'wall_top.png', tip: "верхняя стена", key: 'b'},
    'bw': {color: 'black', bg: 'wall_left.png', tip: "левая стена", key: 'bw'},
    'be': {color: 'black', bg: 'wall_right.png', tip: "правая стена", key: 'be'},
    'bs': {color: 'black', bg: 'wall_bottom.png', tip: "нижняя стена", key: 'bs'},
    'p': {color: 'black', bg: 'wood.png', tip: "глухая стена", key: 'p'},
    'g': {color: 'brown', bg: 'floor.png', tip: "ground", key: 'g'},
    'k': {color: 'gray', bg: 'knight.png', tip: "knight", key: 'k'},
    'y': {color: 'green', bg: 'book.png', tip: "book", key: 'y'},
}
const keys = Object.keys(types)

export default {
    types: types,
    keys: keys
}